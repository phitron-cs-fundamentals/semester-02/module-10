#include <bits/stdc++.h>
using namespace std;
int main()
{
    // list<int>list2 = {10, 20, 30, 40};
    // list<int>myList(10, 5);
    // list<int>myList(list2);
    // int a[5] = {10, 20, 40, 30, 4};
    // list<int>myList(a,a+5);

    vector<int> v = {100, 200, 300, 400};
    list<int> myList(v.begin(), v.end());
    // for (auto it = myList.begin(); it != myList.end(); it++)
    // {
    //     cout << *it << " ";
    // } //long procedure

    for (int val : myList)
    {
        cout << val << endl;
    } // shortcut way to print
    return 0;
}