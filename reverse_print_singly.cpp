#include <bits/stdc++.h>
using namespace std;
class Node
{
public:
    int val;
    Node *next;
    Node(int val) // constructor creating
    {
        this->val = val;
        this->next = NULL;
    }
};

void print_linked_list(Node *head)
{
    Node *tmp = head;
    while (tmp != NULL)
    {
        cout << tmp->val << " ";
        tmp = tmp->next;
    }
    cout<<endl;
}

// void print_recursion(Node *n) //previous way to reverse
// {
//     if (n == NULL)
//         return;
//     cout << n->val << " ";
//     print_recursion(n->next);
// }

// void print_reverse(Node *n)
// {
//     if (n == NULL)
//         return;

//     print_reverse(n->next);
//     cout << n->val << " ";
// }  //end of reverse previous way

void reverse(Node *&head, Node *cur)
{
    if (cur->next == NULL)
    {
        head = cur;
        return;
    }
    reverse(head, cur->next);
    cur->next->next = cur;
    cur->next = NULL;
}

int main()
{
    Node *head = new Node(50);
    Node *a = new Node(10);
    Node *b = new Node(20);
    Node *c = new Node(30);
    Node *d = new Node(40);
    head->next = a;
    a->next = b; // next method
    b->next = c;
    c->next = d;
    print_linked_list(head); 
    reverse(head, head);
    print_linked_list(head);

    return 0;
}